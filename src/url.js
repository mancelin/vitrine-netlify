export function getPageUrl(basePath, pageSlug) {
	if (basePath !== "/") {
		return `${basePath}/${pageSlug}`
	} else {
		return `/${pageSlug}`
	}
}
